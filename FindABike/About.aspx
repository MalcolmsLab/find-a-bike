﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="FindABike.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <p>Find a bike is a service that allows users to look up where bikes provided by the City of London can be picked up within a suburb. To use the website, go back to the home page and enter a suburb in the text box.</p>
    <p>The information used to find bike pick up points is provided by <a href="https://tfl.gov.uk/">Transport for London</a>.</p>
</asp:Content>
