﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Tfl.Api.Presentation.Entities;
using FindABike.Controller;

namespace FindABike
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Pressed(object sender, EventArgs e)
        {
            if (Suburb.Text != "")
            {
                // Get bike points in the entered suburb.
                List<Place> matches = BikePoint.MatchSuburbs(Suburb.Text);
                
                if (matches.Count() == 0)
                {
                    SuburbResults.Text = "No results found.";
                }
                else
                {
                    // Display results.
                    foreach (Place place in matches)
                    {
                        SuburbResults.Text += place.CommonName;
                        SuburbResults.Text += "<br />";
                    }
                }
            }
            else
                SuburbResults.Text = "Please enter a Suburb.";

        }

        
    }
}