﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FindABike._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-4">
            <h2>Enter a suburb to find the bike locations.</h2>
            <asp:TextBox ID="Suburb" placeholder="Chelsea" runat="server"></asp:TextBox>
            <asp:Button ID="btnSubmit" text="Submit" OnClick="btnSubmit_Pressed" runat="server"></asp:Button>
        </div>
        <div class="col-md-4">
            <br />
            <asp:Literal ID="SuburbResults" runat="server"></asp:Literal>
        </div>
    </div>

</asp:Content>
