﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;
using RestSharp.Authenticators;
using Tfl.Api.Presentation.Entities;
using Newtonsoft.Json;

namespace FindABike.Controller
{
    public class Fetcher
    {
        // Transport For Londen API details.
        private const string _apiEndPointTFL = @"https://api.tfl.gov.uk/";
        private static SimpleAuthenticator _apiAuthenticatorTFL = new SimpleAuthenticator("app_id", "8830b966", "app_key", "fe9be2c6d97b51573701f6e7a02f62fd");

        /// <summary>
        /// Gets all bike points within Londen.
        /// </summary>
        /// <returns>A list containing all bike points place information wihtin Londen.</returns>
        public static List<Place> GetAllBikePoints()
        {
            List<Place> bikePoints = null;

            // Set the base URL.
            RestClient client = new RestClient(_apiEndPointTFL);
            client.Authenticator = _apiAuthenticatorTFL;

            // Set the request perameters.
            RestRequest request = new RestRequest("BikePoint", Method.GET);
            request.AddHeader("Content-Type", "application/json");

            // Get data.
            RestResponse response = (RestResponse)client.Execute(request);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var content = response.Content;

                bikePoints = JsonConvert.DeserializeObject<List<Place>>(content);
            }

            return bikePoints;
        }

        /// <summary>
        /// Gets a specific bike point within Londen.
        /// </summary>
        /// <param name="id">The ID of the bikepoint.</param>
        /// <returns>The bike points place information.</returns>
        public static Place GetBikePoint(string id)
        {
            Place bikePoint = null;

            // Set the base URL.
            RestClient client = new RestClient(_apiEndPointTFL);
            client.Authenticator = _apiAuthenticatorTFL;

            // Set the request perameters.
            RestRequest request = new RestRequest("BikePoint/{id}", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddUrlSegment("id", id);

            // Get data.
            RestResponse response = (RestResponse)client.Execute(request);

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var content = response.Content;

                bikePoint = JsonConvert.DeserializeObject<Place>(content);
            }

            return bikePoint;
        }
    }
}