﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tfl.Api.Presentation.Entities;
using System.Text.RegularExpressions;

namespace FindABike.Controller
{
    public class BikePoint
    {
        /// <summary>
        /// Finds any bike stations within the entered suburb.
        /// </summary>
        /// <returns>A list of the places that contain the suburb.</returns>
        public static List<Place> MatchSuburbs(string suburb)
        {
            List<Place> matches = new List<Place>();

            List<Place> places = Fetcher.GetAllBikePoints();

            // Match end of string as the common name ends in the suburb with the suburb name.
            string pattern = string.Format(".+, {0}*$", suburb.ToLower());
            Regex regex = new Regex(pattern);

            foreach (Place place in places)
            {
                if (regex.IsMatch(place.CommonName.ToLower()))
                {
                    matches.Add(place);
                }
            }

            return matches;
        }
    }
}